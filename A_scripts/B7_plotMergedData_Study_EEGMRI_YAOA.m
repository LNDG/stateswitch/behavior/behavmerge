% Grab merged data containing all subjects and sessions, select data to
% plot and plot.

% There is renewed control over the IDs to be included here, as we will
% redetermine the subjects to be included.

% 180328 | include all groups and sessions in single script
%        | select only subjects with complete EEG and MRI data

clear all; clc;

%% paths

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/';
pn.dataIn       = [pn.root, 'B_data/'];
pn.bars         = [pn.root, 'D_tools/barwitherr/']; addpath(pn.bars);
pn.brewer       = [pn.root, 'D_tools/brewermap/']; addpath(pn.brewer);

%% define general IDs: keep only those subjects with complete EEG and MRI data

% N = 42 YA, 53 OA

IDs_Complete = {'1117';'1118';'1120';'1124';'1126';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281';'2104';'2107';'2108';'2112';'2118';'2120';'2121';'2123';'2125';'2129';'2130';'2131';'2132';'2133';'2134';'2135';'2139';'2140';'2145';'2147';'2149';'2157';'2160';'2201';'2202';'2203';'2205';'2206';'2209';'2210';'2211';'2213';'2214';'2215';'2216';'2217';'2219';'2222';'2224';'2226';'2227';'2236';'2237';'2238';'2241';'2244';'2246';'2248';'2250';'2251';'2252';'2258';'2261'};

IDs_YA = IDs_Complete(startsWith(IDs_Complete,'1'));
IDs_OA = IDs_Complete(startsWith(IDs_Complete,'2'));

load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat')

info.ageGroups = {'YA'; 'OA'};
info.dataTypes = {'EEG'; 'MRI'};

setup.OutlierCorrection = 0;

for indAge = 1:numel(info.ageGroups)
    for indType = 1:numel(info.dataTypes)

        MergedData = eval(['MergedData', info.dataTypes{indType}]);
        curDataType = info.dataTypes{indType};
        agegroup = info.ageGroups{indAge};
        if strcmp(agegroup, 'YA')
            folder = 'C_studyYA';
            curIDs = IDs_YA;
        elseif strcmp(agegroup, 'OA')
            folder = 'D_studyOA';
            curIDs = IDs_OA;
        end
        for indID = 1:numel(curIDs)
            curIDs_idx(indID) = find(strcmp(IDs_all, curIDs{indID})); % get index of subjects in behavioral matrix
        end; clear indID;

        MergedData.StateOrders = MergedData.StateOrders(:,curIDs_idx);
        MergedData.Atts = MergedData.Atts(:,curIDs_idx);
        MergedData.RTs = MergedData.RTs(:,curIDs_idx);
        MergedData.Accs = MergedData.Accs(:,curIDs_idx);
        MergedData.expInfo = MergedData.expInfo(curIDs_idx);
        
        pn.plotFolder   = [pn.root, 'C_figures/',folder,'/',curDataType,'/']; mkdir(pn.plotFolder);

        N = size(MergedData.StateOrders,2);

        %% plot overview of behavioral measures

        RTsCollected_mean = []; AccCollected_mean = []; RTsCollected_md = []; AccCollected_md = []; RTsCollected_std = []; AccCollected_std = [];
        RTsRemoved = zeros(N,4,4); ACCsRemoved = zeros(N,4,4);
        for indID = 1:N
            for indAtt = 1:4
                for indDim = 1:4
                    targetTrials = find(MergedData.StateOrders(:,indID)==indDim & MergedData.Atts(:,indID)==indAtt);
                    % outlier correction (within subject and condition)
                    if setup.OutlierCorrection == 1
                        % RTs
                        check = 0;
                        while check == 0
                            A = MergedData.RTs(targetTrials,indID);
                            idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                            if ~isempty(idxOutlier)
                                MergedData.RTs(targetTrials(idxOutlier),indID) = NaN;
                                RTsRemoved(indID,indAtt,indDim) = RTsRemoved(indID,indAtt,indDim)+numel(idxOutlier);
                            else check = 1;
                            end
                        end
                        % accuracies
                        check = 0;
                        while check == 0
                            A = MergedData.Accs(targetTrials,indID);
                            idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                            if ~isempty(idxOutlier)
                                MergedData.Accs(targetTrials(idxOutlier),indID) = NaN;
                                ACCsRemoved(indID,indAtt,indDim) = ACCsRemoved(indID,indAtt,indDim)+numel(idxOutlier);
                            else check = 1;
                            end
                        end
                    end
                    % calculate average metrics
                    RTsCollected_mean(indID,indAtt,indDim) = nanmean(MergedData.RTs(targetTrials,indID));
                    AccCollected_mean(indID,indAtt,indDim) = nanmean(MergedData.Accs(targetTrials,indID));
                    RTsCollected_md(indID,indAtt,indDim) = nanmedian(MergedData.RTs(targetTrials,indID));
                    AccCollected_md(indID,indAtt,indDim) = nanmedian(MergedData.Accs(targetTrials,indID));
                    RTsCollected_std(indID,indAtt,indDim) = nanstd(MergedData.RTs(targetTrials,indID));
                    AccCollected_std(indID,indAtt,indDim) = nanstd(MergedData.Accs(targetTrials,indID));
                end
            end
        end

        cmap = brewermap(4,'YlOrRd');

        h = figure('units','normalized','outerposition',[0 0 1 1]);
        axislabels = {'color','direction','size', 'luminance'};
        subplot(2,3,1); 
            avgMeasure = squeeze(nanmean(RTsCollected_mean,1));
            seMeasure = squeeze(std(RTsCollected_mean,[],1)/sqrt(size(RTsCollected_mean,1)));
            bar1 = barwitherr(seMeasure, avgMeasure); title('Mean RTs');
                legend({'1 Cue'; '2 Cues'; '3 Cues'; '4 Cues'}, 'Location','northwest', 'Orientation','horizontal'); legend('boxoff');
                xticklabels(axislabels);
        subplot(2,3,4);
            avgMeasure = squeeze(nanmean(AccCollected_mean,1));
            seMeasure = squeeze(std(AccCollected_mean,[],1)/sqrt(size(AccCollected_mean,1)));
            bar2 = barwitherr(seMeasure, avgMeasure); title('Mean Accuracy');
            xticklabels(axislabels);
            ylim([.5 1]);
        subplot(2,3,2); 
            avgMeasure = squeeze(nanmean(RTsCollected_md,1));
            seMeasure = squeeze(std(RTsCollected_md,[],1)/sqrt(size(RTsCollected_md,1)));
            bar3 = barwitherr(seMeasure, avgMeasure); title('Median RTs');
                xticklabels(axislabels);
        % subplot(2,3,5);
        %     avgMeasure = squeeze(nanmean(AccCollected_md,1));
        %     seMeasure = squeeze(std(AccCollected_md,[],1)/sqrt(size(AccCollected_md,1)));
        %     bar4 = barwitherr(seMeasure, avgMeasure); title('Median Accuracy');
        %     xticklabels(axislabels);
        subplot(2,3,3);
            avgMeasure = squeeze(nanmean(RTsCollected_std,1));
            seMeasure = squeeze(std(RTsCollected_std,[],1)/sqrt(size(RTsCollected_std,1)));
            bar5 = barwitherr(seMeasure, avgMeasure); title('STD RTs');
            xticklabels(axislabels);
        subplot(2,3,6);
            avgMeasure = squeeze(nanmean(AccCollected_std,1));
            seMeasure = squeeze(std(AccCollected_std,[],1)/sqrt(size(AccCollected_std,1)));
            bar6 = barwitherr(seMeasure, avgMeasure); title('STD Accuracy');
            xticklabels(axislabels);

        for indCol = 1:4
            set(bar1(indCol),'FaceColor',cmap(indCol,:))
            set(bar2(indCol),'FaceColor',cmap(indCol,:))
            set(bar3(indCol),'FaceColor',cmap(indCol,:))
        %    set(bar4(indCol),'FaceColor',cmap(indCol,:))
            set(bar5(indCol),'FaceColor',cmap(indCol,:))
            set(bar6(indCol),'FaceColor',cmap(indCol,:))
        end
        set(findall(gcf,'-property','FontSize'),'FontSize',15)

        if setup.OutlierCorrection == 1
            filename = ['STSWD_',curDataType,'_',agegroup,'_Behavioral_OutlierCorr_N', num2str(N)];
        else
            filename = ['STSWD_',curDataType,'_',agegroup,'_Behavioral_N', num2str(N)];
        end
        saveas(h, [pn.plotFolder, filename], 'fig');
        saveas(h, [pn.plotFolder, filename], 'epsc');
        saveas(h, [pn.plotFolder, filename], 'png');

        %% compute RT switch costs by dimension (collapsed across attributes)

        RTs_switch = []; RTs_stay = []; ACCs_switch = []; ACCs_stay = [];
        RTsSwitchRemoved = zeros(N,4); RTsStayRemoved = zeros(N,4); ACCsSwitchRemoved = zeros(N,4); ACCsStayRemoved = zeros(N,4);
        for indID = 1:N
            for indDim = 1:4
                trialsByDim{indDim} = find(MergedData.expInfo{indID}.StateOrder(:,:) == indDim);
                idxCurrentOrder = find(MergedData.expInfo{indID}.StateOrder(:,1) == indDim);
                trialsBySwitch_no{indDim} = find(abs(diff([nan(8,1), ...
                    MergedData.expInfo{indID}.targetAtt(idxCurrentOrder,:)],[],2))==0);
                trialsBySwitch_yes{indDim} = find(abs(diff([nan(8,1), ...
                    MergedData.expInfo{indID}.targetAtt(idxCurrentOrder,:)],[],2))> 0);
                % get indices of switch  & stay trials
                switchTrials{indDim} = trialsByDim{indDim}(trialsBySwitch_yes{indDim});
                stayTrials{indDim} = trialsByDim{indDim}(trialsBySwitch_no{indDim});
            end
            % reshape matrices
            StateOrders = reshape(MergedData.StateOrders(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            Atts = reshape(MergedData.Atts(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            RTs = reshape(MergedData.RTs(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            Accs = reshape(MergedData.Accs(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';    
            % extract switch/nonswitch trials
            for indDim = 1:4 % switching only possible in 2-4-states
                % get all trials belonging to dimension
                RTs_switch_tmp{indID,indDim} = RTs(switchTrials{indDim});
                RTs_stay_tmp{indID,indDim} = RTs(stayTrials{indDim});
                ACCs_switch_tmp{indID,indDim} = Accs(switchTrials{indDim});
                ACCs_stay_tmp{indID,indDim} = Accs(stayTrials{indDim});
                % outlier removal
                if setup.OutlierCorrection == 1
                    check = 0;
                    while check == 0
                        A = RTs_switch_tmp{indID,indDim};
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            RTs_switch_tmp{indID,indDim}(idxOutlier) = NaN;
                            RTsSwitchRemoved(indID,indDim) = RTsSwitchRemoved(indID,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                    check = 0;
                    while check == 0
                        A = RTs_stay_tmp{indID,indDim};
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            RTs_stay_tmp{indID,indDim}(idxOutlier) = NaN;
                            RTsStayRemoved(indID,indDim) = RTsStayRemoved(indID,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                    check = 0;
                    while check == 0
                        A = ACCs_switch_tmp{indID,indDim};
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            ACCs_switch_tmp{indID,indDim}(idxOutlier) = NaN;
                            ACCsSwitchRemoved(indID,indDim) = ACCsSwitchRemoved(indID,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                    check = 0;
                    while check == 0
                        A = ACCs_stay_tmp{indID,indDim};
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            ACCs_stay_tmp{indID,indDim}(idxOutlier) = NaN;
                            ACCsStayRemoved(indID,indDim) = ACCsStayRemoved(indID,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                end
                % calculate average scores
                RTs_switch(indID,indDim) = nanmedian(RTs_switch_tmp{indID,indDim});
                RTs_stay(indID,indDim) = nanmedian(RTs_stay_tmp{indID,indDim});
                ACCs_switch(indID,indDim) = nanmean(ACCs_switch_tmp{indID,indDim});
                ACCs_stay(indID,indDim) = nanmean(ACCs_stay_tmp{indID,indDim});
                clear RTs_switch_tmp RTs_stay_tmp ACCs_switch_tmp ACCs_stay_tmp;
            end
        end % ID loop

        % calculate group averages

        RTs_switch_mean     = nanmean(RTs_switch,1);
        RTs_switch_se       = squeeze(std(RTs_switch,[],1)/sqrt(size(RTs_switch,1)));
        RTs_stay_mean       = nanmean(RTs_stay,1);
        RTs_stay_se         = squeeze(std(RTs_stay,[],1)/sqrt(size(RTs_stay,1)));
        ACCs_switch_mean    = nanmean(ACCs_switch,1);
        ACCs_switch_se      = squeeze(std(ACCs_switch,[],1)/sqrt(size(ACCs_switch,1)));
        ACCs_stay_mean      = nanmean(ACCs_stay,1);
        ACCs_stay_se        = squeeze(std(ACCs_stay,[],1)/sqrt(size(ACCs_stay,1)));
        RTsSwitchStay_mean  = nanmean(RTs_switch-RTs_stay,1);
        RTsSwitchStay_se    = squeeze(std(RTs_switch-RTs_stay,[],1)/sqrt(size(RTs_switch-RTs_stay,1)));
        ACCsSwitchStay_mean = nanmean(ACCs_switch-ACCs_stay,1);
        ACCsSwitchStay_se   = squeeze(std(ACCs_switch-ACCs_stay,[],1)/sqrt(size(ACCs_switch-ACCs_stay,1)));

        h = figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(1,2,1);
        barwitherr([RTs_switch_se;RTs_stay_se;RTsSwitchStay_se], ...
            [RTs_switch_mean;RTs_stay_mean;RTsSwitchStay_mean]); 
        set(gca, 'XTickLabels', {'Switch'; 'Stay'; 'Switch-Stay'});
        legend({'1-State'; '2-State'; '3-State'; '4-State'}); legend('boxoff');
        title('switch-dependent RTs (median)')

        subplot(1,2,2);
        barwitherr([ACCs_switch_se;ACCs_stay_se; ACCsSwitchStay_se], ...
            [ACCs_switch_mean;ACCs_stay_mean; ACCsSwitchStay_mean]); 
        set(gca, 'XTickLabels', {'Switch'; 'Stay'; 'Switch-Stay'});
        legend({'1-State'; '2-State'; '3-State'; '4-State'}); legend('boxoff');
        title('switch-dependent Accuracy (mean)')

        set(findall(gcf,'-property','FontSize'),'FontSize',15)

        if setup.OutlierCorrection == 1
            filename = ['STSWD_',curDataType,'_',agegroup,'_switchCosts_OutlierCorr_N', num2str(N)];
        else
            filename = ['STSWD_',curDataType,'_',agegroup,'_switchCosts_N', num2str(N)];
        end
        saveas(h, [pn.plotFolder, filename], 'fig');
        saveas(h, [pn.plotFolder, filename], 'epsc');
        saveas(h, [pn.plotFolder, filename], 'png');

        %% switch costs by dimension and attribute

        RTs_switch = []; RTs_stay = []; ACCs_switch = []; ACCs_stay = [];
        RTsSwitchByAttRemoved = zeros(N,4,4); RTsStayByAttRemoved = zeros(N,4,4);
        ACCsSwitchByAttRemoved = zeros(N,4,4); ACCsStayByAttRemoved = zeros(N,4,4);
        for indID = 1:N
            % get switch/no switch
            switchMat = NaN(size(MergedData.expInfo{indID}.targetAtt));
            switchMat(abs(diff([nan(32,1), MergedData.expInfo{indID}.targetAtt(:,:)],[],2))==0) = 0;
            switchMat(abs(diff([nan(32,1), MergedData.expInfo{indID}.targetAtt(:,:)],[],2))>0) = 1;
            % get data
            StateOrders = reshape(MergedData.StateOrders(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            Atts = reshape(MergedData.Atts(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            RTs = reshape(MergedData.RTs(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            Accs = reshape(MergedData.Accs(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            % get data for switch/no switch
            for indDim = 1:4 % switching only possible in 2-4-states
                for indAtt = 1:4
                    curSwitch = find(switchMat==1 & MergedData.expInfo{indID}.StateOrder==indDim & MergedData.expInfo{indID}.targetAtt==indAtt);
                    curStay = find(switchMat==0 & MergedData.expInfo{indID}.StateOrder==indDim & MergedData.expInfo{indID}.targetAtt==indAtt);
                    RTs_switch_tmp{indID,indAtt,indDim} = RTs(curSwitch);
                    RTs_stay_tmp{indID,indAtt,indDim} = RTs(curStay);
                    ACCs_switch_tmp{indID,indAtt,indDim} = Accs(curSwitch);
                    ACCs_stay_tmp{indID,indAtt,indDim} = Accs(curStay);
                    if setup.OutlierCorrection == 1
                        check = 0;
                        while check == 0
                            A = RTs_switch_tmp{indID,indAtt,indDim};
                            idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                            if ~isempty(idxOutlier)
                                RTs_switch_tmp{indID,indAtt,indDim}(idxOutlier) = NaN;
                                RTsSwitchByAttRemoved(indID,indAtt,indDim) = RTsSwitchByAttRemoved(indID,indAtt,indDim)+numel(idxOutlier);
                            else check = 1;
                            end
                        end
                        check = 0;
                        while check == 0
                            A = RTs_stay_tmp{indID,indAtt,indDim};
                            idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                            if ~isempty(idxOutlier)
                                RTs_stay_tmp{indID,indAtt,indDim}(idxOutlier) = NaN;
                                RTsStayByAttRemoved(indID,indAtt,indDim) = RTsStayByAttRemoved(indID,indAtt,indDim)+numel(idxOutlier);
                            else check = 1;
                            end
                        end
                        check = 0;
                        while check == 0
                            A = ACCs_switch_tmp{indID,indAtt,indDim};
                            idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                            if ~isempty(idxOutlier)
                                ACCs_switch_tmp{indID,indAtt,indDim}(idxOutlier) = NaN;
                                ACCsSwitchByAttRemoved(indID,indAtt,indDim) = ACCsSwitchByAttRemoved(indID,indAtt,indDim)+numel(idxOutlier);
                            else check = 1;
                            end
                        end
                        check = 0;
                        while check == 0
                            A = ACCs_stay_tmp{indID,indAtt,indDim};
                            idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                            if ~isempty(idxOutlier)
                                ACCs_stay_tmp{indID,indAtt,indDim}(idxOutlier) = NaN;
                                ACCsStayByAttRemoved(indID,indAtt,indDim) = ACCsStayByAttRemoved(indID,indAtt,indDim)+numel(idxOutlier);
                            else check = 1;
                            end
                        end
                    end
                    RTs_switch(indID,indAtt,indDim) = nanmedian(RTs_switch_tmp{indID,indAtt,indDim});
                    RTs_stay(indID,indAtt,indDim) = nanmedian(RTs_stay_tmp{indID,indAtt,indDim});
                    ACCs_switch(indID,indAtt,indDim) = nanmean(ACCs_switch_tmp{indID,indAtt,indDim});
                    ACCs_stay(indID,indAtt,indDim) = nanmean(ACCs_stay_tmp{indID,indAtt,indDim});
                end
            end
        end % ID loop

        h = figure('units','normalized','outerposition',[0 0 1 1]);
        for indAtt = 1:4
            subplot(4,2,(indAtt-1)*2+1);
            % calculate measures
            RTs_switch_mean     = [];
            RTs_switch_mean     = squeeze(nanmean(RTs_switch(:,indAtt,:),1));
            RTs_switch_se       = [];
            RTs_switch_se       = squeeze(std(RTs_switch(:,indAtt,:),[],1)/sqrt(size(RTs_switch(:,indAtt,:),1)));
            RTs_stay_mean       = [];
            RTs_stay_mean       = squeeze(nanmean(RTs_stay(:,indAtt,:),1));
            RTs_stay_se         = [];
            RTs_stay_se         = squeeze(std(RTs_stay(:,indAtt,:),[],1)/sqrt(size(RTs_stay(:,indAtt,:),1)));
            ACCs_switch_mean    = [];
            ACCs_switch_mean    = squeeze(nanmean(ACCs_switch(:,indAtt,:),1));
            ACCs_switch_se      = [];
            ACCs_switch_se      = squeeze(std(ACCs_switch(:,indAtt,:),[],1)/sqrt(size(ACCs_switch(:,indAtt,:),1)));
            ACCs_stay_mean      = [];
            ACCs_stay_mean      = squeeze(nanmean(ACCs_stay(:,indAtt,:),1));
            ACCs_stay_se        = [];
            ACCs_stay_se        = squeeze(std(ACCs_stay(:,indAtt,:),[],1)/sqrt(size(ACCs_stay(:,indAtt,:),1)));
            RTsSwitchStay_mean  = [];
            RTsSwitchStay_mean  = squeeze(nanmean(RTs_switch(:,indAtt,:)-RTs_stay(:,indAtt,:),1));
            RTsSwitchStay_se    = [];
            RTsSwitchStay_se    = squeeze(std(RTs_switch(:,indAtt,:)-RTs_stay(:,indAtt,:),[],1)/sqrt(size(RTs_switch(:,indAtt,:)-RTs_stay(:,indAtt,:),1)));
            ACCsSwitchStay_mean = [];
            ACCsSwitchStay_mean = squeeze(nanmean(ACCs_switch(:,indAtt,:)-ACCs_stay(:,indAtt,:),1));
            ACCsSwitchStay_se   = [];
            ACCsSwitchStay_se   = squeeze(std(ACCs_switch(:,indAtt,:)-ACCs_stay(:,indAtt,:),[],1)/sqrt(size(ACCs_switch(:,indAtt,:)-ACCs_stay(:,indAtt,:),1)));
        % plot
            barwitherr([RTs_switch_se';RTs_stay_se';RTsSwitchStay_se'], ...
            [RTs_switch_mean';RTs_stay_mean';RTsSwitchStay_mean']); set(gca, 'XTickLabels', {'Switch'; 'Stay'; 'Switch-Stay'});
            if indAtt == 1
                legend({'1-State'; '2-State'; '3-State'; '4-State'}); legend('boxoff');
            end
            title([char(axislabels{indAtt}), ': switch-dependent RTs (median)'])

            subplot(4,2,(indAtt-1)*2+2);
            barwitherr([ACCs_switch_se';ACCs_stay_se'; ACCsSwitchStay_se'], ...
            [ACCs_switch_mean';ACCs_stay_mean'; ACCsSwitchStay_mean']); 
            set(gca, 'XTickLabels', {'Switch'; 'Stay'; 'Switch-Stay'});
            title([char(axislabels{indAtt}), ': switch-dependent Accuracy (mean)'])
        end

        if setup.OutlierCorrection == 1
            filename = ['STSWD_',curDataType,'_',agegroup,'_switchCostsByAtt_OutlierCorr_N', num2str(N)];
        else
            filename = ['STSWD_',curDataType,'_',agegroup,'_switchCostsByAtt_N', num2str(N)];
        end
        saveas(h, [pn.plotFolder, filename], 'fig');
        saveas(h, [pn.plotFolder, filename], 'epsc');
        saveas(h, [pn.plotFolder, filename], 'png');

        %% switch costs for correct trials only

        RTs_switch = []; RTs_stay = []; ACCs_switch = []; ACCs_stay = [];
        RTsSwitchRemoved = zeros(N,4); RTsStayRemoved = zeros(N,4); ACCsSwitchRemoved = zeros(N,4); ACCsStayRemoved = zeros(N,4);
        for indID = 1:N
            % reshape matrices
            StateOrders = reshape(MergedData.StateOrders(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            Atts = reshape(MergedData.Atts(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            RTs = reshape(MergedData.RTs(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';
            Accs = reshape(MergedData.Accs(:,indID),size(MergedData.expInfo{indID}.StateOrder,2),size(MergedData.expInfo{indID}.StateOrder,1))';    
            for indDim = 1:4
                trialsByDim{indDim} = find(MergedData.expInfo{indID}.StateOrder(:,:) == indDim);
                idxCurrentOrder = find(MergedData.expInfo{indID}.StateOrder(:,1) == indDim);
                trialsBySwitch_no{indDim} = find([repmat(-1,8,1), abs(diff(MergedData.expInfo{indID}.targetAtt(idxCurrentOrder,:),[],2))]==0 & ...
                    Accs(idxCurrentOrder,:) == 1);
                trialsBySwitch_yes{indDim} = find([repmat(-1,8,1), abs(diff(MergedData.expInfo{indID}.targetAtt(idxCurrentOrder,:),[],2))]> 0 & ...
                    Accs(idxCurrentOrder,:) == 1);
                % get indices of switch  & stay trials
                switchTrials{indDim} = trialsByDim{indDim}(trialsBySwitch_yes{indDim});
                stayTrials{indDim} = trialsByDim{indDim}(trialsBySwitch_no{indDim});
            end
            % extract switch/nonswitch trials
            for indDim = 1:4 % switching only possible in 2-4-states
                % get all trials belonging to dimension
                RTs_switch_tmp{indID,indDim} = RTs(switchTrials{indDim});
                RTs_stay_tmp{indID,indDim} = RTs(stayTrials{indDim});
                ACCs_switch_tmp{indID,indDim} = Accs(switchTrials{indDim});
                ACCs_stay_tmp{indID,indDim} = Accs(stayTrials{indDim});
                % outlier removal
                if setup.OutlierCorrection == 1
                    check = 0;
                    while check == 0
                        A = RTs_switch_tmp{indID,indDim};
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            RTs_switch_tmp{indID,indDim}(idxOutlier) = NaN;
                            RTsSwitchRemoved(indID,indDim) = RTsSwitchRemoved(indID,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                    check = 0;
                    while check == 0
                        A = RTs_stay_tmp{indID,indDim};
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            RTs_stay_tmp{indID,indDim}(idxOutlier) = NaN;
                            RTsStayRemoved(indID,indDim) = RTsStayRemoved(indID,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                    check = 0;
                    while check == 0
                        A = ACCs_switch_tmp{indID,indDim};
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            ACCs_switch_tmp{indID,indDim}(idxOutlier) = NaN;
                            ACCsSwitchRemoved(indID,indDim) = ACCsSwitchRemoved(indID,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                    check = 0;
                    while check == 0
                        A = ACCs_stay_tmp{indID,indDim};
                        idxOutlier = []; idxOutlier = find(isoutlier(A,'mean')); % find outliers > 3STDs from the mean
                        if ~isempty(idxOutlier)
                            ACCs_stay_tmp{indID,indDim}(idxOutlier) = NaN;
                            ACCsStayRemoved(indID,indDim) = ACCsStayRemoved(indID,indDim)+numel(idxOutlier);
                        else check = 1;
                        end
                    end
                end
                % calculate average scores
                RTs_switch(indID,indDim) = nanmedian(RTs_switch_tmp{indID,indDim});
                RTs_stay(indID,indDim) = nanmedian(RTs_stay_tmp{indID,indDim});
                ACCs_switch(indID,indDim) = nanmean(ACCs_switch_tmp{indID,indDim});
                ACCs_stay(indID,indDim) = nanmean(ACCs_stay_tmp{indID,indDim});
                clear RTs_switch_tmp RTs_stay_tmp ACCs_switch_tmp ACCs_stay_tmp;
            end
        end % ID loop

        % calculate group averages

        RTs_switch_mean     = nanmean(RTs_switch,1);
        RTs_switch_se       = squeeze(std(RTs_switch,[],1)/sqrt(size(RTs_switch,1)));
        RTs_stay_mean       = nanmean(RTs_stay,1);
        RTs_stay_se         = squeeze(std(RTs_stay,[],1)/sqrt(size(RTs_stay,1)));
        ACCs_switch_mean    = nanmean(ACCs_switch,1);
        ACCs_switch_se      = squeeze(std(ACCs_switch,[],1)/sqrt(size(ACCs_switch,1)));
        ACCs_stay_mean      = nanmean(ACCs_stay,1);
        ACCs_stay_se        = squeeze(std(ACCs_stay,[],1)/sqrt(size(ACCs_stay,1)));
        RTsSwitchStay_mean  = nanmean(RTs_switch-RTs_stay,1);
        RTsSwitchStay_se    = squeeze(std(RTs_switch-RTs_stay,[],1)/sqrt(size(RTs_switch-RTs_stay,1)));
        ACCsSwitchStay_mean = nanmean(ACCs_switch-ACCs_stay,1);
        ACCsSwitchStay_se   = squeeze(std(ACCs_switch-ACCs_stay,[],1)/sqrt(size(ACCs_switch-ACCs_stay,1)));

        h = figure('units','normalized','outerposition',[0 0 1 1]);
        subplot(1,2,1);
        barwitherr([RTs_switch_se;RTs_stay_se;RTsSwitchStay_se], ...
            [RTs_switch_mean;RTs_stay_mean;RTsSwitchStay_mean]); 
        set(gca, 'XTickLabels', {'Switch'; 'Stay'; 'Switch-Stay'});
        legend({'1-State'; '2-State'; '3-State'; '4-State'}); legend('boxoff');
        title('switch-dependent RTs (median)')

        subplot(1,2,2);
        barwitherr([ACCs_switch_se;ACCs_stay_se; ACCsSwitchStay_se], ...
            [ACCs_switch_mean;ACCs_stay_mean; ACCsSwitchStay_mean]); 
        set(gca, 'XTickLabels', {'Switch'; 'Stay'; 'Switch-Stay'});
        legend({'1-State'; '2-State'; '3-State'; '4-State'}); legend('boxoff');
        title('switch-dependent Accuracy (mean)')

        set(findall(gcf,'-property','FontSize'),'FontSize',15)

        if setup.OutlierCorrection == 1
            filename = ['STSWD_',curDataType,'_',agegroup,'_switchCostsByAtt_OutlierCorr_correctOnly_N', num2str(N)];
        else
            filename = ['STSWD_',curDataType,'_',agegroup,'_switchCostsByAtt_correctOnly_N', num2str(N)];
        end
        saveas(h, [pn.plotFolder, filename], 'fig');
        saveas(h, [pn.plotFolder, filename], 'epsc');
        saveas(h, [pn.plotFolder, filename], 'png');
        
    end
end