clear all; clc;

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/';
pn.data         = [pn.root, 'A_MergeIndividualData/B_data/'];
pn.tools        = [pn.root, 'B_reliabilityAcrossSessions/D_tools/']; addpath(genpath(pn.tools));
pn.plotFolder   = [pn.root, 'B_reliabilityAcrossSessions/C_figures/'];
pn.functions    = [pn.root, 'B_reliabilityAcrossSessions/A_scripts/functions/']; addpath(pn.functions);

load([pn.data, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'], 'MergedDataEEG', 'MergedDataMRI', 'IDs_all');

%% split results by young and old

IDs_group = str2num(cellfun(@(x)x(1), IDs_all));
idx_YA = IDs_group==1;
idx_OA = IDs_group==2;
ageName(idx_YA==1) = {'YA'};
ageName(idx_OA==1) = {'OA'};

attributes = {'color'; 'direction'; 'size'; 'saturation'};

%% TO DO: define overlapping pairs (i.e. where accurate response is identical between options)
% definition: winning option = response 1, losing option = response 1

MergedDataEEG.expInfo{1}.targetOptionRun{1}()