%% assign response agreement of cues to each trial

    % load merged behavioral data
    behavData = load('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/B_data/SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat');

    % get trials where two options (in two-state scenario) align
    % operationalized via the relative response agreement among the cues

    UniqueResponses = NaN(256,102);

    for indID = 1:numel(behavData.MergedDataEEG.expInfo) % loop across subjects
        if ~isempty(behavData.MergedDataEEG.expInfo{indID})
            count = 1;
            for indRun = 1:4 % loop across runs
                for indBlock = 1:8 % loop across blocks
                    for indTrial = 1:8 % loop across trials
                        cuedAttributes = behavData.MergedDataEEG.expInfo{indID}.AttCuesRun{indRun}{indBlock,indTrial};
                        cuedResponses = behavData.MergedDataEEG.expInfo{indID}.HighProbChoiceRun{indRun}{indBlock,indTrial}(cuedAttributes);
                        targetResponse = behavData.MergedDataEEG.expInfo{indID}.targetOptionRun{indRun}(indBlock,indTrial);
                        UniqueResponses(count,indID) = sum(ismember(cuedResponses, targetResponse))/numel(cuedResponses);
                        count = count + 1; % increase trial number;
                    end
                end
            end
        else
            UniqueResponses(:,indID) = NaN;
        end
    end

%% probe differences in RT or Accuracy between trial types (Dim 2 only)
    
    RTs_lowAgreement = []; RTs_highAgreement = []; Acc_lowAgreement = []; Acc_highAgreement = [];
    for indID = 1:size(behavData.MergedDataEEG.RTs,2)
        RTs_lowAgreement(indID) = nanmean(behavData.MergedDataEEG.RTs(UniqueResponses(:,indID)==.5 & behavData.MergedDataEEG.StateOrders(:,indID) == 2,indID),1);
        RTs_highAgreement(indID) = nanmean(behavData.MergedDataEEG.RTs(UniqueResponses(:,indID)==1 & behavData.MergedDataEEG.StateOrders(:,indID) == 2,indID),1);

        Acc_lowAgreement(indID) = nanmean(behavData.MergedDataEEG.Accs(UniqueResponses(:,indID)==.5 & behavData.MergedDataEEG.StateOrders(:,indID) == 2,indID),1);
        Acc_highAgreement(indID) = nanmean(behavData.MergedDataEEG.Accs(UniqueResponses(:,indID)==1 & behavData.MergedDataEEG.StateOrders(:,indID) == 2,indID),1);
    end

    figure; bar([nanmean(RTs_lowAgreement), nanmean(RTs_highAgreement)])

    [~, p] = ttest(RTs_lowAgreement, RTs_highAgreement)


    figure; bar([nanmean(Acc_lowAgreement), nanmean(Acc_highAgreement)])

    [~, p] = ttest(Acc_lowAgreement, Acc_highAgreement)
    
    % use in linear mixed model to highlight that these differences can not
    % excluisvely account for the effect of load?
    
%% load differences while keeping response similarity equal to 1

    RTs_lowAgreement = []; RTs_highAgreement = []; Acc_lowAgreement = []; Acc_highAgreement = [];
    for indID = 1:size(behavData.MergedDataEEG.RTs,2)
        if indID == 29
            RTs_lowAgreement(indID,:) = NaN;
            RTs_highAgreement(indID,:) = NaN;
            Acc_lowAgreement(indID,:) = NaN;
            Acc_highAgreement(indID,:) = NaN;
        else
            for indLoad = 1:4
                RTs_lowAgreement(indID,indLoad) = nanmean(behavData.MergedDataEEG.RTs(UniqueResponses(:,indID)<1 & ...
                    behavData.MergedDataEEG.StateOrders(:,indID) == indLoad,indID),1);
                RTs_highAgreement(indID,indLoad) = nanmean(behavData.MergedDataEEG.RTs(UniqueResponses(:,indID)==1 & ...
                    behavData.MergedDataEEG.StateOrders(:,indID) == indLoad,indID),1);

                Acc_lowAgreement(indID,indLoad) = nanmean(behavData.MergedDataEEG.Accs(UniqueResponses(:,indID)<1 & ...
                    behavData.MergedDataEEG.StateOrders(:,indID) == indLoad,indID),1);
                Acc_highAgreement(indID,indLoad) = nanmean(behavData.MergedDataEEG.Accs(UniqueResponses(:,indID)==1 & ...
                    behavData.MergedDataEEG.StateOrders(:,indID) == indLoad,indID),1);
            end
        end
    end
    
    figure; 
    subplot(1,2,1); 
    bar([nanmean(Acc_highAgreement(:,1)), nanmean(Acc_highAgreement(:,2)), nanmean(Acc_highAgreement(:,3)), nanmean(Acc_highAgreement(:,4)),...
       nanmean(Acc_lowAgreement(:,1)), nanmean(Acc_lowAgreement(:,2)), nanmean(Acc_lowAgreement(:,3)), nanmean(Acc_lowAgreement(:,4))])
    subplot(1,2,2); 
    bar([nanmean(RTs_highAgreement(:,1)), nanmean(RTs_highAgreement(:,2)), nanmean(RTs_highAgreement(:,3)), nanmean(RTs_highAgreement(:,4)),...
        nanmean(RTs_lowAgreement(:,1)), nanmean(RTs_lowAgreement(:,2)), nanmean(RTs_lowAgreement(:,3)), nanmean(RTs_lowAgreement(:,4))])
   
    % Check: non-unique response should not exist for load 1 (weird results for subject 29)
    % subejct 29 has to be removed from this analysis. This subject
    % performed the first two runs twice instead of runs 3+4. The encoding
    % of the cued features is incorrect for this subject
    
 %% load differences L4

    agreement = [1, 3/4, 2/4, 1/4];
    RTs_byAgreement = []; Acc_byAgreement = [];
    for indID = 1:size(behavData.MergedDataEEG.RTs,2)
        if indID == 29
            RTs_byAgreement(indID,:) = NaN;
            Acc_byAgreement(indID,:) = NaN;
        else
            for indAgree = 1:4
                RTs_byAgreement(indID,indAgree) = nanmedian(behavData.MergedDataEEG.RTs(UniqueResponses(:,indID)==agreement(indAgree) & ...
                    behavData.MergedDataEEG.StateOrders(:,indID) == 4,indID),1);
                Acc_byAgreement(indID,indAgree) = nanmean(behavData.MergedDataEEG.Accs(UniqueResponses(:,indID)==agreement(indAgree) & ...
                    behavData.MergedDataEEG.StateOrders(:,indID) == 4,indID),1);
            end
        end
    end
    
    figure; 
    subplot(1,2,1); 
    bar([nanmean(Acc_byAgreement(1:53,1)), nanmean(Acc_byAgreement(1:53,2)), nanmean(Acc_byAgreement(1:53,3)), nanmean(Acc_byAgreement(1:53,4))])
    subplot(1,2,2); 
    bar([nanmean(RTs_byAgreement(1:53,1)), nanmean(RTs_byAgreement(1:53,2)), nanmean(RTs_byAgreement(1:53,3)), nanmean(RTs_byAgreement(1:53,4))])
   
    cl = 2.*[.3 .1 .1];

    h = figure('units','normalized','position',[.1 .1 .2 .2]);
    set(gcf,'renderer','Painters')
    Acc_byAgreement_within = Acc_byAgreement(1:53,:)-nanmean(Acc_byAgreement(1:53,:),2)+repmat(nanmean(nanmean(Acc_byAgreement(1:53,:),2),1),size(Acc_byAgreement(1:53,:),1),1);  
    curData = Acc_byAgreement_within;
    meanY = nanmean(curData,1);    
    errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
    [h1, hError] = barwitherr(errorY, meanY);
    condPairs = [1,2; 2,3; 3,4];
    condPairsLevel = [.97 .93 .9];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(curData(:,condPairs(indPair,1)), curData(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    set(h1(1),'FaceColor',cl);
    set(hError(1),'LineWidth',3);
    box(gca,'off')
    set(gca, 'XTick', [1,2,3,4]);
    set(gca, 'XTickLabels', {'1'; '1/2'; '1/3'; '1/4'});
    xlabel('Correspondence'); ylabel({'Accuracy'})
    % test linear effect
    curData = Acc_byAgreement(1:53,:);
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title({'4 Targets'; ['linear effect: p = ', num2str(round(p,3))]}); 
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    ylim([.6 1]); 
    xticks = get(gca, 'xtick'); xlim([xticks(1)-.75*(xticks(2)-xticks(1)) xticks(end)+.75*(xticks(2)-xticks(1))]);

    cl = 2.*[.3 .1 .1];

    h = figure('units','normalized','position',[.1 .1 .2 .2]);
    set(gcf,'renderer','Painters')
    RTs_byAgreement_within = RTs_byAgreement(1:53,:)-nanmean(RTs_byAgreement(1:53,:),2)+repmat(nanmean(nanmean(RTs_byAgreement(1:53,:),2),1),size(RTs_byAgreement(1:53,:),1),1);  
    curData = RTs_byAgreement_within;
    meanY = nanmean(curData,1);    
    errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
    [h1, hError] = barwitherr(errorY, meanY);
    condPairs = [1,2; 2,3; 3,4];
    condPairsLevel = [.85 .875 .9];
    for indPair = 1:size(condPairs,1)
        % significance star for the difference
        [~, pval] = ttest(curData(:,condPairs(indPair,1)), curData(:,condPairs(indPair,2))); % paired t-test
        % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
        % sigstars on top
        if pval <.05
            mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
        end
    end
    set(h1(1),'FaceColor',cl);
    set(hError(1),'LineWidth',3);
    box(gca,'off')
    set(gca, 'XTick', [1,2,3,4]);
    set(gca, 'XTickLabels', {'1'; '1/2'; '1/3'; '1/4'});
    xlabel('Correspondence'); ylabel({'RT'})
    % test linear effect
    curData = RTs_byAgreement(1:53,:);
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
    [~, p, ci, stats] = ttest(IndividualSlopes);
    title({'4 Targets'; ['linear effect: p = ', num2str(round(p,3))]}); 
    set(findall(gcf,'-property','FontSize'),'FontSize',30)
    ylim([.6 1]); 
    xticks = get(gca, 'xtick'); xlim([xticks(1)-.75*(xticks(2)-xticks(1)) xticks(end)+.75*(xticks(2)-xticks(1))]);
    
    %% differences by load for correspondence of 1
    
    RTs_byLoad = []; Acc_byLoad = [];
    for indID = 1:size(behavData.MergedDataEEG.RTs,2)
        if indID == 29
            RTs_byLoad(indID,:) = NaN;
            Acc_byLoad(indID,:) = NaN;
        else
            for indAgree = 1:4
                RTs_byLoad(indID,indAgree) = nanmedian(behavData.MergedDataEEG.RTs(UniqueResponses(:,indID)==1 & ...
                    behavData.MergedDataEEG.StateOrders(:,indID) == indAgree,indID),1);
                Acc_byLoad(indID,indAgree) = nanmean(behavData.MergedDataEEG.Accs(UniqueResponses(:,indID)==1 & ...
                    behavData.MergedDataEEG.StateOrders(:,indID) == indAgree,indID),1);
            end
        end
    end
    
    figure; 
    subplot(1,2,1); 
    bar([nanmean(RTs_byLoad(1:53,1)), nanmean(RTs_byLoad(1:53,2)), nanmean(RTs_byLoad(1:53,3)), nanmean(RTs_byLoad(1:53,4))])
    subplot(1,2,2); 
    bar([nanmean(Acc_byLoad(1:53,1)), nanmean(Acc_byLoad(1:53,2)), nanmean(Acc_byLoad(1:53,3)), nanmean(Acc_byLoad(1:53,4))])
   
    addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/RainCloudPlots/'))
    addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/eeg/task/B_analyses/S1_ERPs/T_tools/')
    
cl = 2.*[.3 .1 .1];

h = figure('units','normalized','position',[.1 .1 .2 .2]);
set(gcf,'renderer','Painters')
Acc_byLoad_within = Acc_byLoad(1:53,:)-nanmean(Acc_byLoad(1:53,:),2)+repmat(nanmean(nanmean(Acc_byLoad(1:53,:),2),1),size(Acc_byLoad(1:53,:),1),1);  
curData = Acc_byLoad_within;
meanY = nanmean(curData,1);    
errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
[h1, hError] = barwitherr(errorY, meanY);
condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [.97 .93 .9];
for indPair = 1:size(condPairs,1)
    % significance star for the difference
    [~, pval] = ttest(curData(:,condPairs(indPair,1)), curData(:,condPairs(indPair,2))); % paired t-test
    % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
    % sigstars on top
    if pval <.05
        mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
    end
end
set(h1(1),'FaceColor',cl);
set(hError(1),'LineWidth',3);
box(gca,'off')
set(gca, 'XTick', [1,2,3,4]);
set(gca, 'XTickLabels', {'1'; '2'; '3'; '4'});
xlabel('Targets'); ylabel({'Accuracy'})
% test linear effect
curData = Acc_byLoad(1:53,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title({'Correspondence: 1'; ['linear effect: p = ', num2str(round(p,3))]}); 
set(findall(gcf,'-property','FontSize'),'FontSize',30)
ylim([.7 1]); 
xticks = get(gca, 'xtick'); xlim([xticks(1)-.75*(xticks(2)-xticks(1)) xticks(end)+.75*(xticks(2)-xticks(1))]);

cl = 2.*[.3 .1 .1];

h = figure('units','normalized','position',[.1 .1 .2 .2]);
set(gcf,'renderer','Painters')
RTs_byLoad_within = RTs_byLoad(1:53,:)-nanmean(RTs_byLoad(1:53,:),2)+repmat(nanmean(nanmean(RTs_byLoad(1:53,:),2),1),size(RTs_byLoad(1:53,:),1),1);  
curData = RTs_byLoad_within;
meanY = nanmean(curData,1);    
errorY = nanstd(curData,[],1)/sqrt(size(curData,1));
[h1, hError] = barwitherr(errorY, meanY);
condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [.8 .9 1];
for indPair = 1:size(condPairs,1)
    % significance star for the difference
    [~, pval] = ttest(curData(:,condPairs(indPair,1)), curData(:,condPairs(indPair,2))); % paired t-test
    % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
    % sigstars on top
    if pval <.05
        mysigstar(gca, [condPairs(indPair,1)+.1 condPairs(indPair,2)-.1], condPairsLevel(indPair), pval);
    end
end
set(h1(1),'FaceColor',cl);
set(hError(1),'LineWidth',3);
box(gca,'off')
set(gca, 'XTick', [1,2,3,4]);
set(gca, 'XTickLabels', {'1'; '2'; '3'; '4'});
xlabel('Targets'); ylabel({'RT'})
% test linear effect
curData = RTs_byLoad(1:53,:);
X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes = b(2,:);
[~, p, ci, stats] = ttest(IndividualSlopes);
title({'Correspondence: 1'; ['linear effect: p = ', num2str(round(p,3))]}); 
set(findall(gcf,'-property','FontSize'),'FontSize',30)
ylim([.2 1]); 
xticks = get(gca, 'xtick'); xlim([xticks(1)-.75*(xticks(2)-xticks(1)) xticks(end)+.75*(xticks(2)-xticks(1))]);

%% estimate interindividual relation between linear effects

X = [1 1; 1 2; 1 3; 1 4]; 
b=X\Acc_byLoad(1:53,:)'; IndividualSlopes_byLoad = b(2,:);
b=X\Acc_byAgreement(1:53,:)'; IndividualSlopes_byAgreement = b(2,:);

figure; scatter(IndividualSlopes_byLoad, IndividualSlopes_byAgreement, 'filled')
[r, p] = corrcoef(IndividualSlopes_byLoad(~isnan(IndividualSlopes_byLoad)), IndividualSlopes_byAgreement(~isnan(IndividualSlopes_byAgreement)))

X = [1 1; 1 2; 1 3; 1 4]; 
b=X\RTs_byLoad(1:53,:)'; IndividualSlopes_byLoad = b(2,:);
b=X\RTs_byAgreement(1:53,:)'; IndividualSlopes_byAgreement = b(2,:);

figure; scatter(IndividualSlopes_byLoad, IndividualSlopes_byAgreement, 'filled')
[r, p] = corrcoef(IndividualSlopes_byLoad(~isnan(IndividualSlopes_byLoad)), IndividualSlopes_byAgreement(~isnan(IndividualSlopes_byAgreement)))

X = [1 1; 1 2; 1 3; 1 4]; 
b=X\Acc_byLoad(1:53,:)'; IndividualSlopes_byLoad = b(2,:);
b=X\RTs_byAgreement(1:53,:)'; IndividualSlopes_byAgreement = b(2,:);

figure; scatter(IndividualSlopes_byLoad, IndividualSlopes_byAgreement, 'filled')
[r, p] = corrcoef(IndividualSlopes_byLoad(~isnan(IndividualSlopes_byLoad)), IndividualSlopes_byAgreement(~isnan(IndividualSlopes_byAgreement)))

X = [1 1; 1 2; 1 3; 1 4]; 
b=X\Acc_byLoad(1:53,:)'; IndividualSlopes_byLoad = b(2,:);
b=X\RTs_byLoad(1:53,:)'; IndividualSlopes_byAgreement = b(2,:);

figure; scatter(IndividualSlopes_byLoad, IndividualSlopes_byAgreement, 'filled')
[r, p] = corrcoef(IndividualSlopes_byLoad(~isnan(IndividualSlopes_byLoad)), IndividualSlopes_byAgreement(~isnan(IndividualSlopes_byAgreement)))
