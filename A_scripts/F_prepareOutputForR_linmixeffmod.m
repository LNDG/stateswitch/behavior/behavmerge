clear all; clc;

pn.root         = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/';
pn.data         = [pn.root, 'A_MergeIndividualData/B_data/'];
pn.tools        = [pn.root, 'B_reliabilityAcrossSessions/D_tools/']; addpath(genpath(pn.tools));
pn.plotFolder   = [pn.root, 'B_reliabilityAcrossSessions/C_figures/'];
pn.functions    = [pn.root, 'B_reliabilityAcrossSessions/A_scripts/functions/']; addpath(pn.functions);

load([pn.data, 'SS_MergedDynamic_EEG_MRI_YA_09-Mar-2018.mat'], 'MergedDataEEG', 'MergedDataMRI', 'IDs_all');

%% split results by young and old

IDs_group = str2num(cellfun(@(x)x(1), IDs_all));
idx_YA = IDs_group==1;
idx_OA = IDs_group==2;
ageName(idx_YA==1) = {'YA'};
ageName(idx_OA==1) = {'OA'};

attributes = {'color'; 'direction'; 'size'; 'saturation'};

%% export EEG output

StateSwitchDynEEG{1,1} = 'subject';
StateSwitchDynEEG{1,2} = 'age';
StateSwitchDynEEG{1,4} = 'att';
StateSwitchDynEEG{1,3} = 'dim';
StateSwitchDynEEG{1,5} = 'RT';
StateSwitchDynEEG{1,6} = 'Acc';
StateSwitchDynEEG{1,7} = 'rpt';

count = 2;
for indID = 1:size(MergedDataEEG.RTs,2)
    for indAtt = 1:4
        for indDim = 1:4
            curStateTrls = find(MergedDataEEG.StateOrders(:,indID)==indDim);
            curAttTrls = find(MergedDataEEG.Atts(:,indID)==indAtt);
            curTrials = intersect(curStateTrls, curAttTrls); % get all trials where both conditions are fulfilled
            sequencePosition = mod(curTrials,8); sequencePosition(sequencePosition==0) = 8;
            for indTrial = 1:numel(curTrials)
                StateSwitchDynEEG{count,1} = indID;
                StateSwitchDynEEG{count,2} = ageName{indID};
                StateSwitchDynEEG{count,3} = attributes{indAtt};
                StateSwitchDynEEG{count,4} = indDim;
                StateSwitchDynEEG{count,5} = MergedDataEEG.RTs(curTrials(indTrial), indID);
                StateSwitchDynEEG{count,6} = MergedDataEEG.Accs(curTrials(indTrial), indID);
                StateSwitchDynEEG{count,7} = sequencePosition(indTrial);
                count = count + 1;
            end
        end
    end
end

StateSwitchDynEEG(1,:) = [];

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/B_data/';
save([pn.dataOut, 'F_LMMdataForR_EEG.mat'], 'StateSwitchDynEEG');

%% export MRI output

StateSwitchDynMRI{1,1} = 'subject';
StateSwitchDynMRI{1,2} = 'age';
StateSwitchDynMRI{1,4} = 'att';
StateSwitchDynMRI{1,3} = 'dim';
StateSwitchDynMRI{1,5} = 'meanRT';
StateSwitchDynMRI{1,6} = 'meanAcc';
StateSwitchDynMRI{1,7} = 'rpt';

count = 2;
for indID = 1:numel(IDs_all)
    for indAtt = 1:4
        for indDim = 1:4
            curStateTrls = find(MergedDataEEG.StateOrders(:,indID)==indDim);
            curAttTrls = find(MergedDataEEG.Atts(:,indID)==indAtt);
            curTrials = intersect(curStateTrls, curAttTrls); % get all trials where both conditions are fulfilled
            sequencePosition = mod(curTrials,8); sequencePosition(sequencePosition==0) = 8;
            for indTrial = 1:numel(curTrials)
                StateSwitchDynMRI{count,1} = indID;
                StateSwitchDynMRI{count,2} = ageName{indID};
                StateSwitchDynMRI{count,3} = attributes{indAtt};
                StateSwitchDynMRI{count,4} = indDim;
                StateSwitchDynMRI{count,5} = MergedDataMRI.RTs(curTrials(indTrial), indID);
                StateSwitchDynMRI{count,6} = MergedDataMRI.Accs(curTrials(indTrial), indID);
                StateSwitchDynMRI{count,7} = sequencePosition(indTrial);
                count = count + 1;
            end
        end
    end
end

StateSwitchDynMRI(1,:) = [];

pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/B_data/';
save([pn.dataOut, 'F_LMMdataForR_MRI.mat'], 'StateSwitchDynMRI');
