#install.packages('R.matlab')
#install.packages('lme4')
library(R.matlab)
library(nlme)

## Age by Dim interaction

# read the data for the ANOVA from MATLAB output

rm(list = ls()) # clear workspace

data = readMat('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/B_data/F_LMMdataForR_EEG.mat')
data = as.data.frame(matrix(unlist(data), nrow=7, byrow=T),stringsAsFactors=TRUE, header = TRUE)
data = t(data) # transpose matrix
data = data.frame(data) # convert to data frame
names(data) = c("ID", "age", "att", "dim", "rt", "acc", "seq")

data$ID = as.factor(data$ID)
data$age = as.factor(data$age)
data$att = as.factor(data$att)
data$dim = as.factor(data$dim)
data$rt = as.numeric(sub(",", ".", data$rt, fixed = TRUE))
data$logrt = log(data$rt)
data$acc = as.numeric(sub(",", ".", data$acc, fixed = TRUE))
data$seq = as.factor(data$seq)

data = data[complete.cases(data), ]

# test age-by-dim interaction

# 2x4 mixed:
# IV between: age
# IV within:  dim
# DV:         rt/acc
aov_age_dim_RT <- aov(logrt ~ age*dim + Error(ID/dim), data=data)
summary(aov_age_dim_RT)

aov_age_dim_Acc <- aov(acc ~ age*dim + Error(ID/dim), data=data)
summary(aov_age_dim_Acc)

aov_age_dim_att_rt <- aov(logrt ~ age*dim*att + Error(ID/(dim*att)), data=data)
summary(aov_age_dim_att_rt)

interaction.plot(data$dim, data$age, data$logrt, fun = function(x) mean(x, na.rm = TRUE))
interaction.plot(data$dim, data$age, data$acc, fun = function(x) mean(x, na.rm = TRUE))

## run model of linear contrasts and 1 vs 234 contrast

## set up linear mixed effects model for stimulus repetition

aov_age_dim_att_seq_rt <- lmer(logrt ~ 1 + seq + (1+logrt|ID), data=data, REML = TRUE)
summary(aov_age_dim_att_seq_rt)

lmeModel = lme(logrt ~ 1 + seq + age + dim + att, random=~1 | ID, correlation=corCompSymm(form=~1|ID), method="ML", data=data)
summaryModel = summary(lmeModel)
lmeModelAnova = anova(lmeModel,type='marginal')
lmeModelAnova
CI = intervals(lmeModel, which = "fixed")
CI

interaction.plot(data$seq, data$age, data$acc, fun = function(x) mean(x, na.rm = TRUE))
interaction.plot(data$seq, data$age, data$logrt, fun = function(x) mean(x, na.rm = TRUE))

##############
## MRI data ##
##############

rm(list = ls()) # clear workspace

data = readMat('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/B_data/F_dataForR_MRI.mat')
data = as.data.frame(matrix(unlist(data), nrow=7, byrow=T),stringsAsFactors=TRUE, header = TRUE)
data = t(data) # transpose matrix
data = data.frame(data) # convert to data frame
names(data) = c("ID", "age", "att", "dim", "rt", "acc", "seq")

data$ID = as.factor(data$ID)
data$age = as.factor(data$age)
data$att = as.factor(data$att)
data$dim = as.factor(data$dim)
data$rt = as.numeric(sub(",", ".", data$rt, fixed = TRUE))
data$acc = as.numeric(sub(",", ".", data$acc, fixed = TRUE))
data$seq = as.factor(data$seq)

# test age-by-dim interaction

# 2x4 mixed:
# IV between: age
# IV within:  dim
# DV:         rt/acc
aov_age_dim_RT <- aov(rt ~ age*dim + Error(ID/dim), data=data)
summary(aov_age_dim_RT)

aov_age_dim_Acc <- aov(acc ~ age*dim + Error(ID/dim), data=data)
summary(aov_age_dim_Acc)

aov_age_dim_att_rt <- aov(rt ~ age*dim*att + Error(ID/(dim*att)), data=data)
summary(aov_age_dim_att_rt)

interaction.plot(data$age, data$dim, data$rt, fun = function(x) mean(x, na.rm = TRUE))
interaction.plot(data$age, data$dim, data$acc, fun = function(x) mean(x, na.rm = TRUE))
