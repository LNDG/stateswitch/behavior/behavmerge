pn.dataOut = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/multimodal/B_crossCorrelations/B_data/';
load([pn.dataOut, 'STSWD_summary.mat'], 'STSWD_summary')

idx_YA = cellfun(@str2num, STSWD_summary.IDs)<2000;
idx_OA = cellfun(@str2num, STSWD_summary.IDs)>2000;

%% plot mean effects using RainCloudPlots
 
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/B_reliabilityAcrossSessions/D_tools/'))
addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/T_tools/RainCloudPlots/'))
addpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/D_DDM/T_tools/')

condPairs = [1,2; 2,3; 3,4];
condPairsLevel = [];
condPairsLevel{1} = [1.2 1.3 1.4];  
condPairsLevel{2} = [1.2 1.3 1.4];  
condPairsLevel{3} = [.9 .92 .94]; 
condPairsLevel{4} = [.9 .92 .94]; 
lims{1} = [0 1.5]; lims{2} = [0 1.5]; lims{3} = [.3 1.1]; lims{4} = [.3 1.1];
%colorm = [43/255 140/255 190/255; 230/255 75/255 50/255; 230/255 75/255 50/255];
colorm = [2.*[.3 .1 .1]; 2.*[.3 .1 .1]; 0.0314, 0.3176, 0.6118; 0.0314, 0.3176, 0.6118]
paramLabels = {'EEG RT'; 'MRI RT'; 'EEG Accuracy'; 'EEG Accuracy'}; 

dataSelect = {'EEGRT'; 'MRIRT'; 'EEGAcc'; 'MRIAcc'};

h = figure('units','normalized','position',[.1 .1 .3 .25]);
set(0, 'DefaultFigureRenderer', 'painters'); hold on;
for indIdx = 1:4
    subplot(2,2,indIdx)
    cla; hold on;

    curData = STSWD_summary.behav.(dataSelect{indIdx});
    curData(curData==0) = NaN;
    curData(idx_YA,:)
    curData = squeeze(curData(idx_YA,:));
    
 % read into cell array of the appropriate dimensions
    data = []; data_ws = [];
    for i = 1:4
        for j = 1:1
            data{i, j} = squeeze(curData(:,i));
            % individually demean for within-subject visualization
            data_ws{i, j} = curData(:,i)-...
                nanmean(curData(:,:),2)+...
                repmat(nanmean(nanmean(curData(:,:),2),1),size(curData(:,:),1),1);
        end
    end

    % IMPORTANT: plot individually centered estimates, stats on uncentered estimates!

    cl = colorm(indIdx,:);

    box off
    cla;
        h_rc = rm_raincloud(data, cl,1);
        % add stats
        for indPair = 1:size(condPairs,1)
            % significance star for the difference
            [~, pval] = ttest(data{condPairs(indPair,1), j}, data{condPairs(indPair,2), j}); % paired t-test
            % if mysigstar gets 2 xpos inputs, it will draw a line between them and the
            % sigstars on top
            if pval <.05
               mysigstar_vert(gca, [condPairsLevel{indIdx}(indPair), condPairsLevel{indIdx}(indPair)], [h_rc.m(condPairs(indPair,1),1).YData, h_rc.m(condPairs(indPair,2),1).YData], pval);

            end
        end
        view([90 -90]);
        axis ij
    box(gca,'off')
    %set(gca, 'YTick', [1,2,3,4]);
    set(gca, 'YTickLabels', {'4'; '3'; '2'; '1'});
    ylabel('Target load'); xlabel(paramLabels{indIdx})
    set(findall(gcf,'-property','FontSize'),'FontSize',20)
    xlim(lims{indIdx}); 
    curYTick = get(gca, 'YTick'); ylim([curYTick(1)-1*(curYTick(2)-curYTick(1)) curYTick(4)+1*(curYTick(2)-curYTick(1))]);
    
    X = [1 1; 1 2; 1 3; 1 4]; b=X\curData'; IndividualSlopes_agree(indIdx,:) = b(2,:);
    [~, p(indIdx), ci, stats] = ttest(IndividualSlopes_agree(indIdx,:));
    title(['linear:', num2str(round(p(indIdx), 3))])    
end

set(findall(gcf,'-property','FontSize'),'FontSize',22)
pn.plotFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/behavior/STSW_dynamic/A_MergeIndividualData/C_figures/';
figureName = 'C_RT_ACC_RCP';
saveas(h, [pn.plotFolder, figureName], 'fig');
saveas(h, [pn.plotFolder, figureName], 'epsc');
saveas(h, [pn.plotFolder, figureName], 'png');

nanmean(IndividualSlopes_agree,2)